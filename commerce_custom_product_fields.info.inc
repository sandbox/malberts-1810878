<?php

/**
 * @file
 * Provides metadata for the product entity.
 */


/**
 * Implements hook_commerce_product_type_insert().
 */
function commerce_custom_product_fields_commerce_product_type_insert($type) {
  //Create the line item type field when creating a new line item.
  commerce_custom_product_fields_configure_product_type($type['type']);
}

/**
 * Implements hook_entity_property_info_alter() on top of the Product module.
 */
function commerce_custom_product_fields_entity_property_info_alter(&$info) {
  if (!empty($info['commerce_product']['bundles'])) {
    $properties = array();

    foreach ($info['commerce_product']['bundles'] as $bundle => $bundle_info) {
      $bundle_info += array('properties' => array());
      $properties += $bundle_info['properties'];
    }

    if (!empty($properties['commerce_fields_override'])) {
      $info['commerce_product']['properties']['commerce_fields_override'] = $properties['commerce_fields_override'];
    }
  }
}
