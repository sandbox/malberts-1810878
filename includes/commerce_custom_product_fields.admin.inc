<?php

/**
 * @file
 * Page callbacks and form builder functions for administering line item fields.
 */


/**
 * Menu callback to build the line item fields form for a product type.
 *
 * @param $type
 *   The product type.
 *
 */
function commerce_custom_product_fields_product_type_field_overview_form_wrapper($type) {
  if (is_array($type)) {
    $product_type = $type;
  }
  else {
    $product_type = commerce_product_type_load($type);
  }

  $line_item_type = $product_type['line_item_type'];

  module_load_include('inc', 'field_ui', 'field_ui.admin');

  // The first two arguments are needed by field_ui_field_overview_form().
  return drupal_get_form('commerce_custom_product_fields_overview_form', 'commerce_line_item', $line_item_type, $product_type);
}

/**
 * Menu callback to build the line item fields form for a product.
 * 
 * @param $product
 *   The product.
 */
function commerce_custom_product_fields_product_field_overview_form_wrapper($product) {
  if (is_int($product)) {
    $product = commerce_product_load($product);
  }

  $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
  $line_item_type = $product_wrapper->line_item_type->value();
  $product_type = commerce_product_type_load($product->type);

  module_load_include('inc', 'field_ui', 'field_ui.admin');
  
  // The first two arguments are needed by field_ui_field_overview_form().
  return drupal_get_form('commerce_custom_product_fields_overview_form', 'commerce_line_item', $line_item_type, $product_type, $product);
}

/**
 * Form wrapper callback for commerce_custom_product_fields_overview_form.
 *
 * Modify the form before sending it to field_ui_field_overview() and altering
 * it later.  
 */
function commerce_custom_product_fields_field_overview_form_wrapper($form, &$form_state, $entity_type, $bundle, $product_type, $product = NULL) {
  if (!is_array($product_type)) {
    $product_type = commerce_product_type_load($product_type);
  }
  
  if (empty($product)) {
    $line_item_type = commerce_line_item_type_load($product_type['line_item_type']);
    $line_item_type_source = 'product_type';
    $fields_override = $product_type['line_item_fields_override'];
    $inherit_from = t('line item type: <em>@type_name</em>', array('@type_name' => $line_item_type['name']));
  }
  else {
    $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
    
    $line_item_type = commerce_line_item_type_load($product_wrapper->line_item_type->value());
    $line_item_type_source = $product_wrapper->line_item_type_source->value();
    $fields_override = $product_wrapper->commerce_fields_override->value();
    $inherit_from = t('line item type: <em>@type_name</em>', array('@type_name' => $line_item_type['name']));
  }

  $form['fields_override'] = array(
    '#type' => 'item',
    '#title' => t('Overridden'),
    '#markup' => $fields_override ? t('Yes') : t('No. Settings inherited from !inherit_from', array('!inherit_from' => $inherit_from)),
    '#weight' => -50,
  );

  if (module_exists('field_group')) {
    $form['notice_field_group'] = array(
      '#type' => 'item',
      '#title' => t('Field groups notice'),
      '#markup' =>
        t('Enabling/disabling groups, changing their weights or moving fields outside them will not be saved.') . '<br />' .
        t('By default the field group will only be displayed on the Add to Cart form if at least one child field is enabled.'),
      '#weight' => -40,
    );
  }
  
  $form['#product_type'] = $product_type;
  $form['#product'] = $product;
  $form['#fields_override'] = $fields_override;
  $form['#line_item_type'] = $line_item_type;
  $form['#line_item_type_source'] = $line_item_type_source;

  return $form;
}

/**
 * Field UI field overview form modifications.
 *
 * Called during commerce_custom_product_fields_form_field_ui_field_overview_form_alter().
 */
function commerce_custom_product_fields_field_ui_field_overview(&$form, &$form_state, $display_overview = FALSE) {
  // Do not alter the regular use of field_ui_field_overview_form.
  if (!empty($form['#product_type'])) {
    $reset_weights = !empty($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] == 'reset_weights';
    
    $product_type = $form['#product_type'];
    $product = $form['#product'];
    $fields_override = $form['#fields_override'];

    // Always commerce_line_item.
    $entity_type = $form['#entity_type'];
    $bundle = $form['#bundle'];

    $instances = field_info_instances($entity_type, $bundle);
    $field_types = field_info_field_types();
    $widget_types = field_info_widget_types();

    $extra_fields = field_info_extra_fields($entity_type, $bundle, 'form');

    if (!empty($product)) {
      $parent_type = 'product';
      $parent_type_value = $product->product_id;
    }
    elseif (!empty($product_type)) {
      $parent_type = 'product_type';
      $parent_type_value = $product_type['type'];
    }
    
    $params = array(
      'parent_type' => $parent_type,
      'parent_type_value' => $parent_type_value,
      'line_item_type' => $bundle,
    );
    $line_item_instances = commerce_custom_product_fields_line_item_read_instances($params, TRUE);

    $table = &$form['fields'];

    // Remove header columns.
    $remove_header = array(
      3, // Machine name
      4, // Field type
      6, // Operations
    );

    // Remove unwanted headers.
    foreach ($remove_header as $cell) {
      unset($table['#header'][$cell]);
    }

    $table['#header']['enabled'] = t('Enabled');

    // Remove extra fields.
    foreach($extra_fields as $name => $extra_field) {
      unset($table[$name]);
    }
    $form['#extra'] = array();

    // Modify fields.
    $form['#fields'] = array();
    foreach ($instances as $name => $instance) {
      // Should this field be displayed on the Add to Cart form.
      $table[$name]['#commerce_cart_settings'] = commerce_cart_field_instance_access_settings($instance);
      $field_access = $table[$name]['#commerce_cart_settings']['field_access'];
      $enabled = !empty($line_item_instances[$name]);

      if ($field_access) {
        $form['#fields'][] = $name;
        
        $remove_cells = array(
          // field_ui
          'field_name',
          'type',
          'edit',
          'delete',
        );

        // Hide unwanted columns.
        foreach ($remove_cells as $cell) {
          if (isset($table[$name][$cell])) {
            $table[$name][$cell]['#type'] = 'value';
          }
        }
        
        // Change the values of existing cells.
        $table[$name]['widget_type'] = array(
          '#type' => 'item',
          '#markup' => t($widget_types[$instance['widget']['type']]['label']),
        );

        // Add enabled checkbox.
        $table[$name]['enabled'] = array(
          '#type' => 'checkbox', 
          '#title' => t('Enable @title field', array('@title' => ($instance['label']))), 
          '#title_display' => 'invisible', 
          '#default_value' => $enabled,
        );

        // Keep the original Field UI weight value so we can reset to it later.
        if (!isset($table[$name]['original_weight'])) {
          $table[$name]['original_weight'] = array(
            '#type' => 'value',
            '#value' => $table[$name]['weight']['#default_value'],
          );
        }

        if ($reset_weights) {
          $table[$name]['weight']['#value'] = $table[$name]['original_weight']['#value'];
        }
        elseif ($enabled && isset($line_item_instances[$name]['weight'])) {
          $table[$name]['weight']['#default_value'] = $line_item_instances[$name]['weight'];
        }
      }
      else {
        unset($table[$name]);
      }
    }

    $table['#prefix'] = '<div id="fields-wrapper">';
    $table['#suffix'] = '</div>';

    // Modify field groups.
    if (module_exists('field_group')) {
      $remove_cells = array(
        'group_name',
        'format',
        'settings_summary',
        'settings_edit',
        'delete'
      );

      form_load_include($form_state, 'inc', 'field_group', 'field_group.field_ui');
      $params = field_group_field_ui_form_params($form, $display_overview);
      $formatter_options = field_group_field_formatter_options($display_overview ? 'display' : 'form');

      foreach (array_keys($params->groups) as $name) {
        if (isset($form_state['field_group'][$name])) {
          $group = & $form_state['field_group'][$name];
        }
        else {
          $group = & $params->groups[$name];
        }

        // Hide unwanted columns.
        foreach ($remove_cells as $cell) {
          if (isset($table[$name][$cell])) {
            $table[$name][$cell]['#type'] = 'value';
          }
        }
        
        $table[$name]['format'] = array(
          'type' => array(
            '#type' => 'item',
            '#markup' => $formatter_options[$group->format_type],
          ),
        );

        // Add enabled checkbox.
        $table[$name]['enabled'] = array(
          '#type' => 'checkbox', 
          '#title' => t('Enable @title field', array('@title' => (t($group->label)))), 
          '#title_display' => 'invisible', 
          '#default_value' => FALSE,
          '#disabled' => TRUE,
        );
      }
    }
    
    unset($form['additional_settings']);
    unset($table['#regions']['add_new']);

    $form['reset_weights'] = array(
      '#type' => 'button',
      '#name' => 'reset_weights',
      '#value' => t('Reset weights to line item defaults'),
      // @todo: ajax causes an error!
      /*'#ajax' => array(
        'callback' => 'commerce_custom_product_fields_ajax_field_ui_overview_reset',
        'wrapper' => 'fields-wrapper',
        'method' => 'replace',
        'effect' => 'fade',
      ),*/
    );

    // We need to handle submission and validation.
    if ($fields_override) {
      $form['#submit'] = array('commerce_custom_product_fields_field_overview_submit');
      $form['actions']['submit']['#value'] = t('Save field settings');
    }
    else {
      unset($form['actions']['submit']);
    }
  }
}

/**
 * Menu callback for AJAX additions. Render the new field weights.
 */
function commerce_custom_product_fields_ajax_field_ui_overview_reset($form, $form_state) {
  return $form['fields'];
}

function commerce_custom_product_fields_field_overview_submit($form, &$form_state) {
  $form_values = $form_state['values']['fields'];
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];

  // Save field settings.
  $product_type = $form['#product_type'];
  $product = $form['#product'];

  if (!empty($product)) {
    $parent_type = 'product';
    $parent_type_value = $product->product_id;
  }
  elseif (!empty($product_type)) {
    $parent_type = 'product_type';
    $parent_type_value = $product_type['type'];
  }

  // Collect children (for field_group)
  $children = array_fill_keys($form['#groups'], array());
    
  foreach ($form_values as $key => $values) {
    if (in_array($key, $form['#fields'])) {
      $line_item_instance = commerce_custom_product_fields_line_item_read_instance($key, $bundle, $parent_type, $parent_type_value);

      // Update field enabled status and weight.
      if (!empty($line_item_instance['id'])) {
        $line_item_instance['weight'] = $values['weight'];
        
        if (!empty($values['enabled'])) {
          // Update line item instance.
          commerce_custom_product_fields_line_item_update_instance($line_item_instance);
        }
        else {
          // Delete line item instance.
          commerce_custom_product_fields_line_item_delete_instance($line_item_instance);
        }
      }
      else {
        if (!empty($values['enabled'])) {
          // Create line item instance.
          $instance = field_read_instance('commerce_line_item', $key, $bundle);
          $line_item_instance = array(
            'parent_type' => $parent_type,
            'parent_type_value' => $parent_type_value,
            'instance_id' => $instance['id'],
            'field_name' => $instance['field_name'],
            'line_item_type' => $bundle,
            'weight' => $values['weight'],
          );

          try {
            commerce_custom_product_fields_line_item_create_instance($line_item_instance);
          }
          catch (Exception $e) {
            drupal_set_message(t('There was a problem creating line item field instance %label: @message.', array('%label' => $instance['label'], '@message' => $e->getMessage())), 'error');
          }
        }
      }
    }
  }

  // @todo: implement group data saving.
  
  drupal_set_message(t('Your settings have been saved.'));
}
