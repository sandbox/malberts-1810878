<?php

/**
 * @file
 * Line Item Field CRUD API, handling line item field instance settings.
 */


/**
 * Creates an instance of a line item field.
 *
 * @param $instance
 *   A field instance definition array. The following properties are required:
 *   - parent_type: 'product_type' or 'product'
 *   - parent_type_value: machine name for 'product_type' or product id for 'product'
 *   - line_item_type: the line item type
 *   - field_name: machine name of the field
 *   Other properties, if present, will be written to the 'data' property.
 *
 * @return
 *   The $instance array with the id property filled in.
 *
 * @throws FieldException
 */
function commerce_custom_product_fields_line_item_create_instance($instance) {
  $field = field_read_field($instance['field_name']);
  $field_instance = field_info_instance('commerce_line_item', $instance['field_name'], $instance['line_item_type']);
  
  if (empty($field)) {
    throw new FieldException(t("Attempt to create an instance of a field @field_name that doesn't exist or is currently inactive.", array('@field_name' => $instance['field_name'])));
  }
  
  // Check that the required properties exists.
  if (empty($instance['parent_type'])) {
    throw new FieldException(t('Attempt to create an instance of field @field_name without a parent type.', array('@field_name' => $instance['field_name'])));
  }
  if (empty($instance['parent_type_value'])) {
    throw new FieldException(t('Attempt to create an instance of field @field_name without a parent type value.', array('@field_name' => $instance['field_name'])));
  }
  if (empty($instance['line_item_type'])) {
    throw new FieldException(t('Attempt to create an instance of field @field_name without a line item type.', array('@field_name' => $instance['field_name'])));
  }
  
  // Check that the field can be attached to this entity type.
  if (!empty($field['entity_types']) && !in_array('commerce_line_item', $field['entity_types'])) {
    throw new FieldException(t('Attempt to create an instance of field @field_name on forbidden entity type @entity_type.', array('@field_name' => $instance['field_name'], '@entity_type' => 'commerce_line_item')));
  }
  
  $instance['instance_id'] = $field_instance['id'];

  // Ensure the field instance is unique within the bundle.
  // We only check for instances of active fields, since adding an instance of
  // a disabled field is not supported.
  $prior_instance = commerce_custom_product_fields_line_item_read_instance($instance['field_name'], $instance['line_item_type'], $instance['parent_type'], $instance['parent_type_value']);
  if (!empty($prior_instance)) {
    $message = t('Attempt to create an instance of line item field @field_name on line item type @line_item_type that already has an instance of that field.', array('@field_name' => $instance['field_name'], '@line_item_type' => $instance['line_item_type']));
    throw new FieldException($message);
  }

  _commerce_custom_product_fields_line_item_write_instance($instance);

  // @todo: We should probably have a cache.
  // Clear caches
  //field_cache_clear();

  return $instance;
}

/**
 * Updates an instance of a line item field.
 *
 * @param $instance
 *   An associative array representing an instance structure. The required
 *   keys and values are:
 *   - parent_type: 'product_type' or 'product'
 *   - parent_type_value: machine name for 'product_type' or product id for 'product'
 *   - line_item_type: the line item type
 *   - field_name: machine name of the field
 *   Read-only_id properties are assigned automatically. Any other
 *   properties specified in $instance overwrite the existing values for
 *   the instance.
 *
 * @throws FieldException
 *
 * @see field_create_instance()
 */
function commerce_custom_product_fields_line_item_update_instance($instance) {
  // Check that the specified field exists.
  $field = field_read_field($instance['field_name']);
  if (empty($field)) {
    throw new FieldException(t('Attempt to update an instance of a nonexistent field @field.', array('@field' => $instance['field_name'])));
  }

  // Check that the line item field instance exists (even if it is inactive,
  // since we want to be able to replace inactive widgets with new ones).
  $prior_instance = commerce_custom_product_fields_line_item_read_instance($instance['field_name'], $instance['line_item_type'], $instance['parent_type'], $instance['parent_type_value']);
  if (empty($prior_instance)) {
    throw new FieldException(t("Attempt to update an instance of field @field on line item type @line_item_type that doesn't exist.", array('@field' => $instance['field_name'], '@line_item_type' => $instance['line_item_type'])));
  }

  $instance['id'] = $prior_instance['id'];
  $instance['instance_id'] = $prior_instance['instance_id'];

  _commerce_custom_product_fields_line_item_write_instance($instance, TRUE);

  // @todo: We should probably have a cache.
  // Clear caches.
  //field_cache_clear();
}

/**
 * Stores an instance record in the line item instance configuration database.
 *
 * @param $instance
 *   An instance structure.
 * @param $update
 *   Whether this is a new or existing instance.
 */
function _commerce_custom_product_fields_line_item_write_instance($instance, $update = FALSE) {
  $field = field_read_field($instance['field_name']);
  $field_type = field_info_field_types($field['type']);

  // The serialized 'data' column contains everything from $instance that does
  // not have its own column and is not automatically populated when the
  // instance is read.
  $data = $instance;
  unset($data['id'], $data['instance_id'], $data['field_name'], $data['line_item_type']);

  $record = array(
    'instance_id' => $instance['instance_id'],
    'field_name' => $instance['field_name'],
    'line_item_type' => $instance['line_item_type'],
    'data' => $data,
  );

  switch ($instance['parent_type']) {
    case 'product_type':
      $table = 'commerce_product_type_line_item_field_instance';
      $record['product_type'] = $instance['parent_type_value'];
      break;

    case 'product':
      $table = 'commerce_product_line_item_field_instance';
      $record['product_id'] = $instance['parent_type_value'];
      break;
  }
  
  // We need to tell drupal_update_record() the primary keys to trigger an
  // update.
  if ($update) {
    $record['id'] = $instance['id'];
    $primary_key = array('id');
  }
  else {
    $primary_key = array();
  }
  drupal_write_record($table, $record, $primary_key);
}

/**
 * Reads a single line item instance record from the database.
 * 
 * @param $field_name
 *   The field name.
 * @param $line_item_type
 *   The line item type.
 * @param $parent_type
 *   'product_type' or 'product'
 * @param $parent_type_value
 *   The value for $parent_type to query against.
 * 
 * @returns 
 *   An instance structure, or FALSE.
 */
function commerce_custom_product_fields_line_item_read_instance($field_name, $line_item_type, $parent_type, $parent_type_value) {
  $params = array(
    'field_name' => $field_name,
    'line_item_type' => $line_item_type,
    'parent_type' => $parent_type,
    'parent_type_value' => $parent_type_value,
  );
  
  $instances = commerce_custom_product_fields_line_item_read_instances($params);
  return $instances ? current($instances) : FALSE;
}

/**
 * Reads in field instances that match an array of conditions.
 *
 * @param $param
 *   An array of properties to use in selecting a field
 *   instance. Valid keys include any column of the
 *   commerce_product_line_item_field_instance and
 *   commerce_product_type_line_item_field_instance tables.
 * @param $key_fieldname
 *   Whether to index the resulting array by the field_name or not.
 * @return
 *   An array of instances matching the arguments.
 */
function commerce_custom_product_fields_line_item_read_instances($params, $key_fieldname = FALSE) {
  $parent_type = $params['parent_type'];
  
  switch ($parent_type) {
    case 'product_type':
      $table = 'commerce_product_type_line_item_field_instance';
      $params['product_type'] = $params['parent_type_value'];
      break;

    case 'product':
      $table = 'commerce_product_line_item_field_instance';
      $params['product_id'] = $params['parent_type_value'];
      break;
  }
  
  unset($params['parent_type']);
  unset($params['parent_type_value']);
  
  $query = db_select($table, 'lfi', array('fetch' => PDO::FETCH_ASSOC));
  //$query->join('field_config_instance', 'fci', 'fci.id = lfi.instance_id');
  //$query->join('field_config', 'fc', 'fc.id = fci.field_id');
  $query->fields('lfi');
  //$query->fields('fci', array('field_id'));

  // Turn the conditions into a query.
  foreach ($params as $key => $value) {
    $query->condition('lfi.' . $key, $value);
  }

  $instances = array();
  $results = $query->execute();

  foreach ($results as $record) {
    // Filter out instances on unknown entity types (for instance because the
    // module exposing them was disabled).
    $entity_info = entity_get_info('commerce_line_item');
    if ($entity_info) {
      $instance = unserialize($record['data']);
      $instance['id'] = $record['id'];
      $instance['instance_id'] = $record['instance_id'];
      $instance['field_name'] = $record['field_name'];
      $instance['line_item_type'] = $record['line_item_type'];
      $instance['parent_type'] = $parent_type;

      switch ($parent_type) {
        case 'product_type':
          $instance['product_type'] = $record['product_type'];
          break;

        case 'product':
          $instance['product_id'] = $record['product_id'];
          break;
      }

      if ($key_fieldname) {
        $instances[$instance['field_name']] = $instance;
      }
      else {
        $instances[] = $instance;
      }
    }
  }
  return $instances;
}

/**
 * Delete a line item field instance.
 *
 * @param $instance
 *   An instance structure.
 */
function commerce_custom_product_fields_line_item_delete_instance($instance) {
  switch ($instance['parent_type']) {
    case 'product_type':
      $table = 'commerce_product_type_line_item_field_instance';
      break;

    case 'product':
      $table = 'commerce_product_line_item_field_instance';
      break;
  }
  
  // We want to delete here because we only store enabled fields.
  db_delete($table)
  ->condition('id', $instance['id'])
  ->execute();

  // Clear the cache.
  //field_cache_clear();
}
