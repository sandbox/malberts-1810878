<?php

/**
 * @file
 * Contains Drupal Commerce specific hook implementations.
 */


/**
 * Implements hook_commerce_product_type_info_alter().
 */
function commerce_custom_product_fields_commerce_product_type_info_alter(&$types) {
  foreach ($types as $name => &$type) {
    $instance = field_info_instance('commerce_product', 'commerce_fields_override', $name);

    $instance_settings = commerce_custom_product_fields_field_instance_override_settings($instance);
    $fields_override = $instance_settings['commerce_fields_override'];

    if (!empty($fields_override)) {
      // The value was explicitly set to TRUE
      $type['line_item_fields_override'] = TRUE;
    }
    else {
      // If the value was not set in code, set a default here.
      if (empty($type['line_item_fields_override'])) {
        $type['line_item_fields_override'] = FALSE;
      }
    }
  }
}
